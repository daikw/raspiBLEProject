"use strict";

// BLE requirements
var util = require('util');
var bleno = require('bleno');

var Data_Ca_characteristic = require('../characteristics/data_Ca_characteristic');
var Data_Na_characteristic = require('../characteristics/data_Na_characteristic');
var Data_alb_characteristic = require('../characteristics/data_alb_characteristic');


var DataService = function() {
	bleno.PrimaryService.call(this, {
		uuid: 'f001',
		characteristics: [
			new Data_Ca_characteristic(),
			new Data_Na_characteristic(),
			new Data_alb_characteristic()
		]
	});
};

util.inherits(DataService, bleno.PrimaryService);

module.exports = DataService;