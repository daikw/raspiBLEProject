"use strict";

// BLE requirements
var util = require('util');
var bleno = require('bleno');

var Measure_Ca_characteristic = require('../characteristics/measure_Ca_characteristic');
var Measure_Na_characteristic = require('../characteristics/measure_Na_characteristic');
var Measure_alb_characteristic = require('../characteristics/measure_alb_characteristic');


var MeasureService = function() {
	bleno.PrimaryService.call(this, {
		uuid: 'f000',
		characteristics: [
			new Measure_Ca_characteristic(),
			new Measure_Na_characteristic(),
			new Measure_alb_characteristic()
		]
	});
};

util.inherits(MeasureService, bleno.PrimaryService);

module.exports = MeasureService;