"use strict";

var util = require('util')
var bleno = require('bleno');

var MeasureService = require('./services/measureService');
var DataService = require('./services/dataService');


var name_MS = 'Measure Handler';
var measureService = new MeasureService();

var name_DS = 'Measured Data';
var dataService = new DataService();


console.log('peripheral - initiating');

bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {
    bleno.startAdvertising(name_MS, [measureService.uuid]);
  } else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(error) {
  console.log('advertising of ' + name_MS + ': ' + (error ? 'error ' + error : 'success'));

  if (!error) {
    bleno.setServices([measureService, dataService], function(error){
      console.log('setServices: '  + (error ? 'error ' + error : 'success'));
    });
  }
});
