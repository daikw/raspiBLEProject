"use strict";

// socket通信
var io = require('socket.io-client');
var host = 'localhost'
var port = 37564
var namespace = '/Na_d';

var url = 'http://' + host + ':' + port + namespace;

// BLE requirements
var util = require('util');
var bleno = require('bleno');
var Descriptor = bleno.Descriptor;

// Na stands for sodium ion
var Data_Na_characteristic = function() {
  bleno.Characteristic.call(this, {
    uuid: 'c201',
    properties: ['read', 'notify'],
    value: null,
    descriptors: [
      new Descriptor({
        uuid: 'd001',
        value: 'Na ion in aqeous solution. [M] (This is not implemeted)'
      }),
      new Descriptor({
        uuid: 'd002',
        value: 'Na'
      }),
      new Descriptor({
        uuid: 'd003',
        value: 'log([M])'
      })
    ]
  });

  var socket = new io(url, {reconnect: true})

  var that = this

  socket.on('connect', function (socket) {
      console.log('Connected - data Na characteristics');
  });

  socket.on('measureMidst', function(result) {
    console.log('measureMidst event catched')
    that._value = Buffer(String(result['concentration']));
    if (that._updateValueCallback) {
      console.log('Data_Na_characteristic: notifying');
      that._updateValueCallback(that._value);
    }
  });

  socket.on('measureFinished_Na', function(result) {
    console.log('measureFinished event catched')
    that._value = Buffer(String(result['concentration']));
    if (that._updateValueCallback) {
      console.log('Data_Na_characteristic: notifying');
      that._updateValueCallback(that._value);
    }
  });

  socket.on('disconnect', function() {
      console.log('disconnected - data Na characteristics');
  });
  this._socket = socket

  this._value = new Buffer(0);
  this._updateValueCallback = null;

};

util.inherits(Data_Na_characteristic, bleno.Characteristic);

Data_Na_characteristic.prototype.onReadRequest = function(offset, callback) {
  console.log('Data_Na_characteristic - onReadRequest: value = ' + this._value.toString('hex'));

  callback(this.RESULT_SUCCESS, this._value);
};


Data_Na_characteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('Data_Na_characteristic - onSubscribe');

  this._updateValueCallback = updateValueCallback;
};

Data_Na_characteristic.prototype.onUnsubscribe = function() {
  console.log('Data_Na_characteristic - onUnsubscribe');

  this._updateValueCallback = null;
};

module.exports = Data_Na_characteristic;
