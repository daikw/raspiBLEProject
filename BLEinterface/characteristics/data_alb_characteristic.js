"use strict";

// socket通信
var io = require('socket.io-client');
var host = 'localhost'
var port = 37564
var namespace = '/alb_d';

var url = 'http://' + host + ':' + port + namespace;

// BLE requirements
var util = require('util');
var bleno = require('bleno');
var Descriptor = bleno.Descriptor;

// alb stands for albumin
var Data_alb_characteristic = function() {
  bleno.Characteristic.call(this, {
    uuid: 'c301',
    properties: ['read', 'notify'],
    value: null,
    descriptors: [
      new Descriptor({
        uuid: 'd001',
        value: 'Albumin in urine. [] (by Colorimetry)'
      }),
      new Descriptor({
        uuid: 'd002',
        value: 'Albumin'
      }),
      new Descriptor({
        uuid: 'd003',
        value: 'log([M])'
      })
    ]
  });

  var socket = new io(url, {reconnect: true})

  var that = this

  socket.on('connect', function (socket) {
      console.log('Connected - data albumin characteristics');
  });

  socket.on('measureMidst', function(result) {
    console.log('measureMidst event catched')
    that._value = Buffer(String(result['concentration']));
    if (that._updateValueCallback) {
      console.log('Data_alb_characteristic: notifying');
      that._updateValueCallback(that._value);
    }
  });

  socket.on('measureFinished_alb', function(result) {
    console.log('measureFinished event catched')
    that._value = Buffer(String(result['concentration']));
    if (that._updateValueCallback) {
      console.log('Data_alb_characteristic: notifying');
      that._updateValueCallback(that._value);
    }
  });

  socket.on('disconnect', function() {
      console.log('disconnected - data albumin characteristics');
  });
  this._socket = socket

  this._value = new Buffer(0);
  this._updateValueCallback = null;

};

util.inherits(Data_alb_characteristic, bleno.Characteristic);

Data_alb_characteristic.prototype.onReadRequest = function(offset, callback) {
  console.log('Data_alb_characteristic - onReadRequest: value = ' + this._value.toString('hex'));

  callback(this.RESULT_SUCCESS, this._value);
};


Data_alb_characteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('Data_alb_characteristic - onSubscribe');

  this._updateValueCallback = updateValueCallback;
};

Data_alb_characteristic.prototype.onUnsubscribe = function() {
  console.log('Data_alb_characteristic - onUnsubscribe');

  this._updateValueCallback = null;
};

module.exports = Data_alb_characteristic;
