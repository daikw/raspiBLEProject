"use strict";

// socket通信
var io = require('socket.io-client');
var host = 'localhost'
var port = 37564
var namespace = '/Ca_m';

var url = 'http://' + host + ':' + port + namespace;

// BLE requirements
var util = require('util');
var bleno = require('bleno');
var Descriptor = bleno.Descriptor;

// Ca stands for calcium ion
var Measure_Ca_characteristic = function() {
  bleno.Characteristic.call(this, {
    uuid: 'c100',
    properties: ['read', 'notify'],
    value: null,
    descriptors: [
      new Descriptor({
        uuid: 'd100',
        value: 'Ca measure handler'
      })
    ]
  });

  var socket = new io(url, {reconnect: true})

  var that = this

  socket.on('connect', function (socket) {
      console.log('Connected - measure Ca characteristics');
  });

  socket.on('sleepDurationTime', function(duration) {
    console.log('sleepDurationTime event catched')
    that._value = Buffer(String(duration));
    if (that._updateValueCallback) {
      that._updateValueCallback(that._value);
    }
  });

  socket.on('disconnect', function() {
      console.log('disconnected - measure Ca characteristics');
  });
  this._socket = socket


  this._value = new Buffer(0);
  this._updateValueCallback = null;

};

util.inherits(Measure_Ca_characteristic, bleno.Characteristic);


Measure_Ca_characteristic.prototype.onReadRequest = function(offset, callback) {
  console.log('Measure_Ca_characteristic - onReadRequest: value = ' + this._value.toString('hex'));

  callback(this.RESULT_SUCCESS, this._value);
};

Measure_Ca_characteristic.prototype.onSubscribe = async function(maxValueSize, updateValueCallback) {
  console.log('Measure_Ca_characteristic - onSubscribe');

  this._updateValueCallback = updateValueCallback;

  // PythonサーバへstartMeasureイベント送出、測定開始をリクエスト
  this._socket.emit('startMeasure', {'target': 'Ca'});
};

Measure_Ca_characteristic.prototype.onUnsubscribe = function() {
  console.log('Measure_Ca_characteristic - onUnsubscribe');

  this._updateValueCallback = null;
  this._socket.emit('stopMeasure');
};


module.exports = Measure_Ca_characteristic;
