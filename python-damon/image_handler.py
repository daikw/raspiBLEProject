#!/usr/bin/env python
import cv2
import numpy as np


# main function which selects proper function according to 'target'
# Functions return the value of specific color value.
def calc_image(image_file_path, target):
	if target == 'Ca':
		return calc_ca(image_file_path)
	elif target == 'Na':
		return calc_na(image_file_path)
	elif target == 'alb':
		return calc_alb(image_file_path)
	else:
		return 0


def calc_ca(image_file_path):
	# read and preparation of images
	img = cv2.imread(image_file_path)
	bgr = cv2.split(img)
	img_r = bgr[2]

	# ２値化、輪郭取得
	img_r_blurred = cv2.GaussianBlur(img_r, (11, 11), 0)
	ret, thresh = cv2.threshold(img_r_blurred, 20, 255, cv2.THRESH_BINARY)

	#重心取得
	moments = cv2.moments(thresh)
	if moments['m00'] != 0:
		cx, cy = int(moments['m10']/moments['m00']), int(moments['m01']/moments['m00'])
	else:
		cx, cy = len(img_r[0])//2, len(img_r)//2

	# 値を計算
	mask = np.zeros(img_r.shape, np.uint8)
	x, y = cx - 150, cy - 150
	w = h = 300
	rect = np.array([[[x, y]], [[x+w, y]], [[x+w, y+h]], [[x, y+h]]])
	cv2.drawContours(mask, [rect], 0, 255, -1)
	mean_values = cv2.mean(img_r, mask)

	return mean_values[0]


# for debug other functions.
import random
def calc_na(image_file_path):
	return random.randint(0, 100)


# placeholder
def calc_alb(image_file_path):
	# read and preparation of images
	img = cv2.imread(image_file_path)
	bgr = cv2.split(img)
	img_g = bgr[1]

	# use the H value
	hsv = cv2.split(cv2.cvtColor(img, cv2.COLOR_BGR2HSV))
	img_h = hsv[0]

	# binalize
	img_g_blurred = cv2.GaussianBlur(img_g, (15, 15), 0)
	_, thresh = cv2.threshold(img_g_blurred, 50, 255, cv2.THRESH_BINARY)

	# get the center of gravity of G value (which will be used as the center of circle)
	moments = cv2.moments(thresh)
	if moments['m00'] != 0:
	    cx, cy = int(moments['m10']/moments['m00']), int(moments['m01']/moments['m00'])
	else:
	    cx, cy = len(img_g[0])//2, len(img_g)//2

	# generate mask
	mask = np.zeros(img_r.shape, np.uint8)
	cv2.circle(mask, (cx, cy), 130, 255, -1)

	# calculate the mean hue value
	mean_values = cv2.mean(img_h, mask)
	return mean_values[0]
