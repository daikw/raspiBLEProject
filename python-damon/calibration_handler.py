#!/usr/bin/env python
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import os


calib_dir = "calibrations/"


# Main function which selects proper function according to 'target'.
# Functions return the value as double, unit is [M]
def calc_conc(color_value, target):
	if target == 'Ca':
		return calc_ca(color_value)
	elif target == 'Na':
		return calc_na(color_value)
	elif target == 'alb':
		return calc_alb(color_value)
	else:
		return 0


def calc_ca(color_value):
	# load and scaling it into [0, 1]
	calib = pd.read_csv(os.path.join(calib_dir, "ca.csv"))
	y_, x_ = calib['R_value'], calib['conc [log(M)]']

	y_upper = max(y_)
	y_ = y_/y_upper
	y = color_value/y_upper

	if y > 1.00:
		return "less than -6"

	# ca model
	def ca_fit(x, a, b, c, d):
		return  a + b/(1+np.exp((c-x/d)))

	# fitting
	p, c = curve_fit(ca_fit, x_, y_)

	# calculating (inverse function)
	conc = p[3]*(p[2] - np.log(p[1]/(y - p[0]) - 1))
	return round(conc, 2)


# for debug other functions.
def calc_na(color_value):
	return color_value/101*100


def calc_alb(color_value):
	# load and scaling it into [0, 1]
	calib = pd.read_csv(os.path.join(calib_dir, "alb.csv"))
	y_, x_ = calib.iloc[:, 1], calib.iloc[:, 0]

	# alb model
	def ca_fit(x, a, b):
		return  a * np.log(x) + b

	# fitting
	p, c = curve_fit(ca_fit, x_, y_)

	# calculating (inverse function)
	conc = np.exp((color_value - p[1])/p[0])
	return round(conc, 1)
