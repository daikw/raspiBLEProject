#!/usr/bin/env python

# requirements
from threading import Lock
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect

import time
import random
import asyncio

#"""
# original modules
from camera_handler import capture
from camera_handler import get_duration_time
from image_handler import calc_image
from calibration_handler import calc_conc
#"""

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = "gevent"

app = Flask(__name__)
#app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode = async_mode, ping_timeout = 3600)

# requirements
host = 'localhost'
port = 37564
debug_mode = True

# namespaces
measure_ns = {'Ca': '/Ca_m', 'Na': '/Na_m', 'alb': '/alb_m'}
data_ns = {'Ca': '/Ca_d', 'Na': '/Na_d', 'alb': '/alb_d'}


measure_stop_flg = False


# This function calls all of original modules.\
#  if not needed in the case of debug, you can eliminate this from\
#  the event function bundled with the event named "startMeasure"
def measure(msg):
    target = msg['target']
    print('requested target is: ' + target)
    duration = get_duration_time(target)

    print('sleepDurationTime event was emitted')
    emit('sleepDurationTime', duration, namespace = measure_ns[target])

    socketio.sleep(duration)

    image_file_path = capture(target)
    print('image:',  image_file_path)

    color_value = calc_image(image_file_path, target)
    print('color_valu:', color_value)

    concentration = calc_conc(color_value, target)
    print('conc:', concentration)

    emit('measureFinished_'+target, {'concentration': concentration}, namespace = data_ns[target])
    print('measureFinished event was emitted')


# these event will be triggered by tapping button of iOS app
#  which has written the measureCharacteristic.
@socketio.on('startMeasure', namespace = measure_ns['Ca'])
def sm_ca(msg):
    print('startMeasure event catched of Ca by daemon.py')
    measure(msg)

@socketio.on('startMeasure', namespace = measure_ns['Na'])
def sm_na(msg):
    print('startMeasure event catched of Na by daemon.py')
    measure(msg)

@socketio.on('startMeasure', namespace = measure_ns['alb'])
def sm_alb(msg):
    print('startMeasure event catched of alb by daemon.py')
    measure(msg)


@socketio.on('stopMeasure')
def stop_measure():
    print('stopMeasure event catched by daemon.py')
    measure_stop_flg = True


@socketio.on('connect')
def test_connect():
    print('connection event catched by daemon.py')


@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected', request.sid)


if __name__ == '__main__':
    socketio.run(app, host = host, port = port, debug = not debug_mode)
