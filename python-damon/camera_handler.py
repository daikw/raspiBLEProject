#!/usr/bin/env python

import json
import os
import picamera as pc
import RPi.GPIO as GPIO
import time
import datetime as dt


def get_present_camera_parameters(picam):
	a = float(picam.analog_gain)
	ec = picam.exposure_compensation
	es = picam.exposure_speed
	g_red, g_blue = picam.awb_gains
	ss = picam.shutter_speed
	fr = picam.framerate
	iso = picam.iso
	return [capture_count, t, a, g_red, g_blue, ec, es, fr, iso]


def get_duration_time(target):
	with open("camera_parameters.json", "r") as jf:
		params = json.load(jf)[target]['camera']
	return params['d']


def capture(target):
	if target in ('Ca', 'Na'):
		return capture_fluorescence(target)
	elif target in ('alb',):
		return capture_colorimetry(target)
	else:
		return capture_wrapper_debug(target)


def capture_colorimetry(target):

	# GPIO settings
	GPIO.setmode(GPIO.BCM)
	gp_out = 20
	GPIO.setup(gp_out, GPIO.OUT)

	# save directory and path
	savedir = "/home/dlab-pi/measuredData/" + dt.datetime.now().strftime("%Y_%m_%d") + "/"
	try:
		os.makedirs(savedir)
	except:
		pass

	image_file_path = savedir + target + ': ' + time.ctime().replace(' ', '_') + '.jpg'

	# load parameters defined in json
	with open("camera_parameters.json", "r") as jf:
		params = json.load(jf)[target]
		c_params = params['camera']
		l_params = params['led']

		# led parameters
		led_state = l_params['on']

		# camera parameters
		iso_set = c_params['iso']
		sm_set = c_params['sm']
		fr_set = c_params['fr']
		d = c_params['d']
		waiting_time = c_params['wt']

	# initialize
	GPIO.output(gp_out, led_state)
	cm = pc.PiCamera(sensor_mode = sm_set, framerate=fr_set)

	# Set ISO to the desired value
	cm.iso = iso_set

	# Wait for the automatic gain control to settle
	time.sleep(waiting_time)

	# values of camera settings
	cm.shutter_speed = cm.exposure_speed
	cm.exposure_mode = 'off'
	g = cm.awb_gains
	cm.awb_mode = 'off'
	cm.awb_gains = g

	# take photo with the fixed settings
	cm.capture(image_file_path, format = 'jpeg')

	# closing
	cm.close()
	GPIO.output(gp_out, 0)
	GPIO.cleanup()

	return image_file_path


def capture_fluorescence(target):

	# GPIO settings
	GPIO.setmode(GPIO.BCM)
	gp_out = 20
	GPIO.setup(gp_out, GPIO.OUT)

	# save directory and path
	savedir = "/home/dlab-pi/measuredData/" + dt.datetime.now().strftime("%Y_%m_%d") + "/"
	try:
		os.makedirs(savedir)
	except:
		pass

	image_file_path = savedir + target + ': ' + time.ctime().replace(' ', '_') + '.jpg'

	# load parameters defined in json
	with open("camera_parameters.json", "r") as jf:
		params = json.load(jf)[target]
		c_params = params['camera']
		l_params = params['led']

		# led parameters
		led_state = l_params['on']

		# camera parameters
		iso_set = c_params['iso']
		sm_set = c_params['sm']
		g_set = tuple(c_params['g'])
		d = c_params['d']
		waiting_time = c_params['wt']

	# initialize
	GPIO.output(gp_out, led_state)
	cm = pc.PiCamera(sensor_mode = sm_set)

	# Set ISO to the desired value
	cm.iso = iso_set

	# Wait for the automatic gain control to settle
	time.sleep(waiting_time)

	# values of camera settings
	cm.shutter_speed = cm.exposure_speed
	cm.exposure_mode = 'off'

	cm.awb_mode = 'off'
	cm.awb_gains = g_set

	# take photo with the fixed settings
	cm.capture(image_file_path, format = 'jpeg')

	# close
	cm.close()
	GPIO.output(gp_out, 0)
	GPIO.cleanup()

	return image_file_path


def capture_wrapper_debug(target):
	return 10
